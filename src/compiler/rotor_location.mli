open Containers

include module type of Compiler_import.Location

(* We alias the error_of_exn function to maintain the signature in compiler
   versions >= 4.06.0 *)
val err_of_exn : exn -> error option

val pp_error : error Format.printer
(** A printer for [Location.error]s *)

val equal : t -> t -> bool
open Containers
open   Format

open Compiler_import
open   Lexing
open   Location

include Location

let equal p p' =
     String.equal p.pos_fname p'.pos_fname
  && p.pos_lnum = p'.pos_lnum
  && p.pos_bol = p'.pos_bol
  && p.pos_cnum = p'.pos_cnum

let equal l l' =
     equal l.loc_start l'.loc_start
  && equal l.loc_end l'.loc_end
  && Bool.equal l.loc_ghost l'.loc_ghost

#if OCAML_MINOR < 6

let err_of_exn = error_of_exn

#else

let err_of_exn ex =
  match error_of_exn ex with
  | Some `Already_displayed -> Some (error "")
  | Some `Ok err            -> Some err
  | None                    -> None

#endif

#if OCAML_MINOR < 8
let rec pp_error fmt { loc; msg; sub; _ } =
  let () = fprintf fmt "@[<v 2>%s: %a" msg print_loc loc in
  let () = sub |> List.iter @@ fprintf fmt "@,%a" pp_error in
  fprintf fmt "@]" 
#else
let pp_error fmt { kind; main; sub } =
  let print_msg fmt { txt; loc } =
    let () = txt fmt in
    fprintf fmt ": %a" print_loc loc in
  let () = fprintf fmt "@[<v 2>%a" print_msg main in
  let () = sub |> List.iter @@ fprintf fmt "@,%a" print_msg in
  fprintf fmt "@]" 
#endif
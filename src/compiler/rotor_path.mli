open Compiler_import

include module type of struct include Path end

val drop : t -> t -> Longident.t option
val to_longident : t -> Longident.t

val dest_ident : t -> Ident.t
val dest_dot   : t -> t * string
val dest_apply : t -> t * t

val dot_map : ?hd:(t -> t) -> ?tl:(string -> string) -> t -> t

val all_prefixes : ?self:bool -> t -> t list
open Compiler

open Asttypes
open Location

open Lib
open Ast_lib
open Visitors_lib
open Refactoring_error
open Sourcefile

let name = "Rename Expression Identifier"
let to_repr () =
  failwith "Rename_ident.get_repr: Does not have a representation!"
let get_deps ~mli _ = Refactoring_deps.Elt.Set.empty

let kernel_mem _ = true
let kernel = Codebase.to_list

let from_id : string option ref = ref None
let to_id : string option ref = ref None

let set_from id = from_id := Some id
let set_to id = to_id := Some id

let initialise args =
  if List.length args < 2 then
    raise (Invalid_argument (name ^ ": Not enough parameters!"))
  else
  set_from (List.nth args 0) ;
  set_to (List.nth args 1)

let from_id () = match !from_id with
  | Some id -> id
  | None ->
    let err = Not_initialised "Identifier to substitute for not specified!" in
    raise (Error (err, None))
let to_id () = match !to_id with
  | Some id -> id
  | None ->
    let err = Not_initialised "Identifier to substitute not specified!" in
    raise (Error (err, None))

let gen_replacement (id : Longident.t) (loc : Location.t) =
  let from_id = from_id () in
  let to_id = to_id () in
  let _id = longident_to_string id in
  match String.equal from_id _id, loc with
  | true, { loc_ghost = false; _ } ->
      Replacement.Set.singleton
        (Replacement.mk loc to_id)
  | _ -> Replacement.Set.empty

let reducer = object
    inherit [_] Refactoring_visitors.result_reducer
    method! visit_Pexp_ident env { txt; loc } = gen_replacement txt loc
    method! visit_Texp_ident env _ { txt; loc } _ = gen_replacement txt loc
  end

let process_file f =
  match f.ast with
  | Interface (Some _sig, _) ->
      reducer#visit_pt_signature () _sig
  | Implementation (Some _struct, _) ->
      reducer#visit_pt_structure () _struct
  | _ -> raise (Error (Bad_sourcefile "No AST to process!", None))
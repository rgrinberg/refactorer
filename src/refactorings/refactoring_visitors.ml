open Containers

open Compiler

open Asttypes
open Typedtree

open Elements
open Elements.Base
open Types_views
open Moduletype

open Identifier

open Ast_utils

class ['self] result_reducer = object (self : 'self)
  inherit ['self] Typedtree_visitors.reduce
  inherit Replacement.Set.monoid
end

class ['self] repr_reducer = object (self : 'self)
  inherit ['self] Typedtree_visitors.reduce
  inherit Refactoring_repr.Set.monoid
end

class ['self] dep_reducer = object (self : 'self)
  inherit ['self] Typedtree_visitors.reduce
  inherit Refactoring_deps.Elt.Set.monoid
end

class ['a] id_env id = object(self)
  val _id : 'a Chain.t = id
  method id = _id
  method with_id new_id = {< _id = new_id >}
end

class module_scope_env scope = object (self)
  val _scope : Modulescope.t = scope
  method scope = _scope
  method with_scope scope = {< _scope = scope >}
end

class dep_descr_env descr = object (self)
  val _descr : Refactoring_deps.descr option = descr
  method dep_descr = _descr
  method with_dep_descr new_descr = {< _descr = Some new_descr >}
end

class virtual ['self] reduce_with_id = object (self : 'self)

  inherit ['self] Typedtree_visitors.reduce as super

  method! visit_tt_structure env this =
    let Atom.Ex hd = Chain.hd env#id in
    match lookup this.str_final_env (ST this.str_type) (Chain.mk hd) with
    | None ->
      failwith
        (Format.sprintf "Did not find a local binding for %a" Chain.pp env#id)
    | Some type_data ->
      let id = item_ident type_data in
      let id_atm = Atom.map (fun _ -> id) hd in
      match find_binding id_atm (Typedtree_views.Str this) with
      | None ->
        failwith
          (Format.sprintf
            "Did not find the local binding of %a" Printtyp.ident id)
      | Some binding ->
        self#process_binding_from_struct env this (id_atm, type_data) binding

  method! visit_tt_signature env this =
    (* We shouldn't expect that the .mli file contain a declaration for the
        value, as it might not be exposed at the top level. *)
    let Atom.Ex hd = Chain.hd env#id in
    match lookup this.sig_final_env (ST this.sig_type) (Chain.mk hd) with
    | None ->
      self#zero
    | Some type_data ->
      let id = item_ident type_data in
      let id_atm = Atom.map (fun _ -> id) hd in
      match find_binding id_atm (Typedtree_views.Sig this) with
      | None ->
        failwith @@ Format.sprintf
          "Did not find the local binding of %a" Printtyp.ident id
      | Some binding ->
        self#process_binding_from_sig env this (id_atm, type_data) binding

  method process_binding_from_struct
      : 'b .  < id : 'a Chain.t; .. >
                -> Typedtree.structure
                -> ((Ident.t, 'b) Atom.t * 'b item_element_view)
                -> ('b, impl) binding_source
                -> 'c
      = fun env _struct _ _ ->
    super#visit_tt_structure env _struct

  method process_binding_from_sig
      : 'b .  < id : 'a Chain.t; .. >
                -> Typedtree.signature
                -> ((Ident.t, 'b) Atom.t * 'b item_element_view)
                -> ('b, intf) binding_source
                -> 'c
      = fun env _sig _ _ ->
    super#visit_tt_signature env _sig

end
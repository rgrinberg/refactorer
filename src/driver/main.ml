open Containers
open   IO
open   Fun

open Compiler
open   Compmisc
open   Config

open Refactoring
open Refactoring_error
open Refactoring_sigs

open Lib

open Frontend

open Sourcefile
open Fileinfos

let rename from_id to_id (codebase, filedeps) rdeps_file =
  try
    let r = Repr.Rename (from_id, to_id) in
    let (deps, results) = apply r codebase ~filedeps in
    let () = Logging.info @@ fun _f -> _f
      ~header:"Refactoring Set"
      "@[<v>%a@]" (Repr.Set.pp Repr.pp) (Deps.dependents deps) in
    let () =
      match rdeps_file with
      | None -> ()
      | Some f ->
        with_out f @@ fun f ->
          Format.fprintf (Format.formatter_of_out_channel f)
            "@[<v>%a@]@." Deps.pp deps in
    let () =
      Logging.major_progress ~msgf:(fun _f -> _f
        "Requires changes to %i files" (Fileinfos.Map.cardinal results)) () in
    results |> Fileinfos.Map.iter @@ fun f results ->
      let src_file = Sourcefile.of_fileinfos f in
      let () =
        Logging.debug @@ fun _f -> _f
          "Performing the following replacements in %s:@,@[%a@]"
          f.filename (Replacement.Set.pp Replacement.pp) results in
      let results = Replacement.apply_all results src_file.contents in
      let results = Sourcefile.diff src_file results in
      let () =
        Logging.info @@ fun _f -> _f
          "Computed patch for %s" f.filename in
      if String.length results <> 0 then
        Output.print_endline results
  with
  | Error (err, _) ->
    let () = log_error err in
    failwith
      (Format.sprintf "%a" print_error err)

let mod_deps (_, deps) =
  let process_item f =
    let module_name = Fileinfos.cmt_module_name f in
    let sort =
      Option.get_exn
        (String.chop_prefix ~pre:"."
          (Filename.extension (Fileinfos.filename f))) in
    let deps =
      List.map Fileinfos.cmt_module_name (Fileinfos.Graph.succ deps f) in
    let deps = List.sort_uniq String.compare deps in
    Output.print_endline
      (Format.sprintf "@[<h>%s:%s %a@]"
        module_name sort
        (List.pp ~start:"[ " ~stop:" ]" ~sep:" " Format.string) deps) in
  Fileinfos.Graph.iter_vertex process_item deps

let tool_name =
  Filename.basename Sys.executable_name

(* Command Line *)

let dep_file =
  let docs = Cmdliner.Manpage.s_options in
  let doc = "Save refactoring dependencies in $(docv)" in
  let docv = "FILE" in
  let arg_info = Cmdliner.Arg.info ~docs ~doc ~docv ["rdeps"] in
  Cmdliner.Arg.(value & opt (some string) None arg_info)

let rename_term =
  let from_id =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The identifier of the value to be renamed." in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    let id_conv =
      let parser s =
        try 
          Ok (Identifier.of_string s)
        with Failure s | Invalid_argument s -> 
          Error (`Msg s) in
      Cmdliner.Arg.conv (parser, Identifier._pp) in
    Cmdliner.Arg.(required & pos 0 (some id_conv) None arg_info) in
  let to_id =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The identifier to rename to." in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    let id_conv =
      let parser s =
        if Identifier.is_lowercase_ident s then Ok s
        else Error (`Msg "Must rename to a valid identifier!") in
      Cmdliner.Arg.conv (parser, Format.string) in
    Cmdliner.Arg.(required & pos 1 (some id_conv) None arg_info) in
  Cmdliner.Term.(
    const rename
      $ from_id
      $ to_id
      $ (Codebase.of_cmdline_with_deps $ const tool_name)
      $ dep_file)
let rename_info =
  let doc = "Renames a value." in
  Cmdliner.Term.info ~doc "rename"

let mod_deps_term =
  let open Cmdliner.Term in
  const mod_deps $ (Codebase.of_cmdline_with_deps $ const tool_name)
let mod_deps_info =
  let doc = "Output module dependencies of input codebase." in
  Cmdliner.Term.info ~doc "mod-deps"

let cmds = [
    (rename_term, rename_info) ;
    (mod_deps_term, mod_deps_info) ;
  ]

let cmds = List.map (Pair.map1 (with_common_opts)) cmds

let help_secs = [
    `S Cmdliner.Manpage.s_common_options;
    `P "These options are common to all commands.";
    `S "MORE HELP";
    `P "Use `$(mname) $(i,COMMAND) --help' for help on a single command.";`Noblank;
    `S Cmdliner.Manpage.s_bugs;
    `P "Check bug reports at https://gitlab.com/trustworthy-refactoring/refactorer/issues.";
  ]

let default_cmd =
  let doc = "a refactoring tool for OCaml" in
  let sdocs = Cmdliner.Manpage.s_common_options in
  let exits = Cmdliner.Term.default_exits in
  let man = help_secs in
  let version = Configuration.version in
  Cmdliner.Term.(
    ret (const (fun _ -> `Help (`Pager, None))
      $ with_common_opts (Codebase.of_cmdline_with_deps $ const tool_name))),
  Cmdliner.Term.info tool_name ~version ~doc ~sdocs ~exits ~man

let () = Cmdliner.Term.(exit @@ eval_choice default_cmd cmds)

open Containers

let out_file : string option ref = ref None

let output_file =
  let docs = Cmdliner.Manpage.s_common_options in
  let doc = "Write standard output to $(docv)." in
  let docv = "FILE" in
  let arg_info = Cmdliner.Arg.info ~docs ~doc ~docv ["o"] in
  Cmdliner.Arg.(value & opt (some string) None arg_info)

let set f =
  IO.with_out f (fun oc -> output oc Bytes.empty 0 0) ;
  out_file := Some f

let print_string s =
  match !out_file with
  | None ->
    print_string s
  | Some f ->
    IO.with_out_a f (fun oc -> output_string oc s)

let print_endline s =
  match !out_file with
  | None ->
    print_endline s
  | Some f ->
    IO.with_out_a f (fun oc -> output_string oc s) ;
    IO.with_out_a f (fun oc -> output_char oc '\n')
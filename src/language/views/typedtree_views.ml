open Containers

open Compiler

open Asttypes
open Typedtree

open Elements
open Elements.Base
open Identifier

open Lib

type _ root_view =
  | Str : structure -> impl root_view
  | Sig : signature -> intf root_view

type _ item_view =
  | InStr : structure_item -> impl item_view
  | InSig : signature_item -> intf item_view

type _ items_view =
  | InStr : structure_item list -> impl items_view
  | InSig : signature_item list -> intf items_view

type _ value_view =
  | InStrVal  : value_binding -> impl value_view
  | InStrPrim : value_description -> impl value_view
  | InSig     : value_description -> intf value_view

(* type _ structure_view =
  | InStr : module_binding -> impl structure_view
  | InSig : module_declaration -> intf structure_view

type _ functor_view =
  | InStr : module_binding -> impl functor_view
  | InSig : module_declaration -> intf functor_view *)

type _ module_view =
  | InStr : module_binding -> impl module_view
  | InSig : module_declaration -> intf module_view

(* type _ structure_type_view =
  | InStr : module_type_declaration -> impl structure_type_view
  | InSig : module_type_declaration -> intf structure_type_view

type _ module_type_view =
  | InStr : module_type_declaration -> impl module_type_view
  | InSig : module_type_declaration -> intf module_type_view *)

type _ module_expr_view =
  | InStr : module_expr -> impl module_expr_view
  | InSig : module_type -> intf module_expr_view

type _ module_type_view =
  | InStr : module_type_declaration -> impl module_type_view
  | InSig : module_type_declaration -> intf module_type_view

type _ include_view =
  | InStr : include_declaration -> impl include_view
  | InSig : include_description -> intf include_view

type (_, _) element_view =
  | Value         : 'b value_view       -> (_value, 'b) element_view
  | Structure     : 'b module_view      -> (_structure, 'b) element_view
  | Functor       : 'b module_view      -> (_functor, 'b) element_view
  | StructureType : 'b module_type_view -> (_structure_type, 'b) element_view
  | FunctorType   : 'b module_type_view -> (_functor_type, 'b) element_view

type _ path_view =
  | Value         : Path.t -> _value path_view
  | Structure     : Path.t -> _structure path_view
  | Functor       : Path.t -> _functor path_view
  | StructureType : Path.t -> _structure_type path_view
  | FunctorType   : Path.t -> _functor_type path_view

type _ item_description_ctxt =
  | InStr : {
      str_loc : Location.t ;
      str_env : Env.t ;
    } -> impl item_description_ctxt
  | InSig : {
      sig_loc : Location.t ;
      sig_env : Env.t ;
    } -> intf item_description_ctxt

type _ item_ctxt =
  | InStr : structure_item list * structure_item list -> impl item_ctxt
  | InSig : signature_item list * signature_item list -> intf item_ctxt

type _ item_list_ctxt =
  | InStr : {
      str_type : Types.signature ;
      str_final_env : Env.t ;
    } -> impl item_list_ctxt
  | InSig : {
      sig_type : Types.signature ;
      sig_final_env : Env.t ;
    } -> intf item_list_ctxt

type _ value_ctxt =
  | InStrVal :
      rec_flag * (Typedtree.value_binding list * Typedtree.value_binding list)
        ->      impl value_ctxt
  | InStrPrim : impl value_ctxt
  | InSig :     intf value_ctxt

type _ module_ctxt =
  | InStr : (module_binding list * module_binding list) Option.t
      -> impl module_ctxt
  | InSig : (module_declaration list * module_declaration list) Option.t
      -> intf module_ctxt

type _ module_type_ctxt =
  | InStr : impl module_type_ctxt
  | InSig : intf module_type_ctxt

type (_, _) item_element =
  | Value         : 'a value_ctxt  * 'a value_view       -> (_value,          'a) item_element
  | Structure     : 'a module_ctxt * 'a module_view      -> (_structure,      'a) item_element
  | Functor       : 'a module_ctxt * 'a module_view      -> (_functor,        'a) item_element
  | StructureType :                  'a module_type_view -> (_structure_type, 'a) item_element
  | FunctorType   :                  'a module_type_view -> (_functor_type,   'a) item_element


let sort : type a . a root_view -> a sort =
  function
  | Str _ -> Impl
  | Sig _ -> Intf

let item_list_ctxt : type a . a root_view -> a item_list_ctxt =
  function
  | Str { str_type; str_final_env; _ } ->
    InStr { str_type; str_final_env; }
  | Sig { sig_type; sig_final_env; _ } ->
    InSig { sig_type; sig_final_env; }

let item_description_ctxt : type a . a item_view -> a item_description_ctxt =
  function
  | InStr { str_loc; str_env; _ } ->
    InStr { str_loc; str_env; }
  | InSig { sig_loc; sig_env; _ } ->
    InSig { sig_loc; sig_env; }

let item_ctxt
      : type a . a sort -> a item_view list * a item_view list -> a item_ctxt =
  fun sort items ->
    match sort with
    | Impl ->
      let xs, ys =
        Pair.map_same (List.map (fun ((InStr x) : a item_view) -> x)) items in
      InStr (xs, ys)
    | Intf ->
      let xs, ys =
        Pair.map_same (List.map (fun ((InSig x) : a item_view) -> x)) items in
      InSig (xs, ys)

let items : type a . a root_view -> a item_view list =
  function
  | Str { str_items; _ } ->
    List.map (fun item -> ((InStr item) : impl item_view)) str_items
  | Sig { sig_items; _ } ->
    List.map (fun item -> ((InSig item) : intf item_view)) sig_items

let check_pattern id pat =
  let reducer = object(self)
    inherit [_] Typedtree_visitors.reduce
    method zero = false
    method plus = (||)
    method! visit_Tpat_var id id' _ =
      Ident.same id id'
    method! visit_Tpat_alias id pat as_id _ =
      Ident.same id as_id
        ||
      self#visit_tt_pattern id pat
  end in
  reducer#visit_tt_pattern id pat

let item_element :
    type a b .
      (Ident.t, a) Atom.t -> b item_view -> (a, b) item_element Option.t =
  fun id item ->
    match id, item with
    | Atom.Value id,
      InStr { str_desc = Tstr_value (rec_flag, bindings); _ } ->
      bindings
      |> pivot_pred ~rev:false (fun { vb_pat; _ } -> check_pattern id vb_pat)
      |> Option.map (fun (l, binding, r) ->
          Value (InStrVal (rec_flag, (l, r)), InStrVal binding))
    | Atom.Value id,
      InStr { str_desc = Tstr_primitive ({ val_id; _ } as desc); _ }
        when Ident.same id val_id ->
      Some (Value (InStrPrim, InStrPrim desc))
    | Atom.Value id, InSig { sig_desc = Tsig_value ({ val_id; _ } as desc); _ }
        when Ident.same id val_id ->
      Some (Value (InSig, InSig desc))
    | Atom.Structure id,
      InStr { str_desc = Tstr_module ({ mb_id; _ } as mb); _ }
        when Ident.same id mb_id ->
      Some (Structure (InStr None, InStr mb))
    | Atom.Structure id, InStr { str_desc = Tstr_recmodule mbs; _ } ->
      mbs
        |> pivot_pred (fun mb -> Ident.same id mb.mb_id)
        |> Option.map (fun (l, mb, r)
        -> Structure (InStr (Some (l, r)), InStr mb))
    | Atom.Structure id,
      InSig { sig_desc = Tsig_module ({ md_id; _ } as mb); _ }
        when Ident.same id md_id ->
      Some (Structure (InSig None, InSig mb))
    | Atom.Structure id, InSig { sig_desc = Tsig_recmodule mds; _ } ->
      mds
        |> pivot_pred (fun md -> Ident.same id md.md_id)
        |> Option.map (fun (l, md, r)
        -> Structure (InSig (Some (l, r)), InSig md))
    | Atom.Functor id,
      InStr { str_desc = Tstr_module ({ mb_id; _ } as mb); _ }
        when Ident.same id mb_id ->
      Some (Functor (InStr None, InStr mb))
    | Atom.Functor id, InStr { str_desc = Tstr_recmodule mbs; _ } ->
      mbs
        |> pivot_pred (fun mb -> Ident.same id mb.mb_id)
        |> Option.map (fun (l, mb, r)
        -> Functor (InStr (Some (l, r)), InStr mb))
    | Atom.Functor id,
      InSig { sig_desc = Tsig_module ({ md_id; _ } as mb); _ }
        when Ident.same id md_id ->
      Some (Functor (InSig None, InSig mb))
    | Atom.Functor id, InSig { sig_desc = Tsig_recmodule mds; _ } ->
      mds
        |> pivot_pred (fun md -> Ident.same id md.md_id)
        |> Option.map (fun (l, md, r)
        -> Functor (InSig (Some (l, r)), InSig md))
    | Atom.StructureType id,
      InStr { str_desc = Tstr_modtype ({ mtd_id; _ } as mtd); _ }
        when Ident.same id mtd_id ->
      Some (StructureType (InStr mtd))
    | Atom.StructureType id,
      InSig { sig_desc = Tsig_modtype ({ mtd_id; _ } as mtd); _ }
        when Ident.same id mtd_id ->
      Some (StructureType (InSig mtd))
    | Atom.FunctorType id,
      InStr { str_desc = Tstr_modtype ({ mtd_id; _ } as mtd); _ }
        when Ident.same id mtd_id ->
      Some (FunctorType (InStr mtd))
    | Atom.FunctorType id,
      InSig { sig_desc = Tsig_modtype ({ mtd_id; _ } as mtd); _ }
        when Ident.same id mtd_id ->
      Some (FunctorType (InSig mtd))
    | _ ->
      None

let initial_env : type a . a root_view -> _ =
  function
  | Str { str_items = []; str_final_env; _ } ->
    str_final_env
  | Str { str_items = { str_env; _ } :: _; _ } ->
    str_env
  | Sig { sig_items = []; sig_final_env; _ } ->
    sig_final_env
  | Sig { sig_items = { sig_env; _ } :: _; _ } ->
    sig_env
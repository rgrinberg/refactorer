open Containers

open Compiler

open Typedtree
open Asttypes

open Elements
open Elements.Base
open Identifier

(* VIEWS *)

type _ root_view =
  | Str : structure -> impl root_view
  | Sig : signature -> intf root_view

type _ item_view =
  | InStr : structure_item -> impl item_view
  | InSig : signature_item -> intf item_view

type _ items_view =
  | InStr : structure_item list -> impl items_view
  | InSig : signature_item list -> intf items_view

type _ value_view =
  | InStrVal  : value_binding     -> impl value_view
  | InStrPrim : value_description -> impl value_view
  | InSig     : value_description -> intf value_view

type _ module_view =
  | InStr : module_binding     -> impl module_view
  | InSig : module_declaration -> intf module_view

type _ module_expr_view =
  | InStr : module_expr -> impl module_expr_view
  | InSig : module_type -> intf module_expr_view

type _ module_type_view =
  | InStr : module_type_declaration -> impl module_type_view
  | InSig : module_type_declaration -> intf module_type_view

type _ include_view =
  | InStr : include_declaration -> impl include_view
  | InSig : include_description -> intf include_view

type (_, _) element_view =
  | Value         : 'b value_view       -> (_value,          'b) element_view
  | Structure     : 'b module_view      -> (_structure,      'b) element_view
  | Functor       : 'b module_view      -> (_functor,        'b) element_view
  | StructureType : 'b module_type_view -> (_structure_type, 'b) element_view
  | FunctorType   : 'b module_type_view -> (_functor_type,   'b) element_view

type _ path_view =
  | Value         : Path.t -> _value path_view
  | Structure     : Path.t -> _structure path_view
  | Functor       : Path.t -> _functor path_view
  | StructureType : Path.t -> _structure_type path_view
  | FunctorType   : Path.t -> _functor_type path_view

(* CONTEXTS *)

type _ item_description_ctxt =
  | InStr : {
      str_loc : Location.t ;
      str_env : Env.t ;
    } -> impl item_description_ctxt
  | InSig : {
      sig_loc : Location.t ;
      sig_env : Env.t ;
    } -> intf item_description_ctxt

type _ item_ctxt =
  | InStr : structure_item list * structure_item list -> impl item_ctxt
  | InSig : signature_item list * signature_item list -> intf item_ctxt

type _ item_list_ctxt =
  | InStr : {
      str_type : Types.signature ;
      str_final_env : Env.t ;
    } -> impl item_list_ctxt
  | InSig : {
      sig_type : Types.signature ;
      sig_final_env : Env.t ;
    } -> intf item_list_ctxt

type _ value_ctxt =
  | InStrVal :
      rec_flag * (Typedtree.value_binding list * Typedtree.value_binding list)
        ->      impl value_ctxt
  | InStrPrim : impl value_ctxt
  | InSig :     intf value_ctxt

type _ module_ctxt =
  | InStr : (module_binding list * module_binding list) Option.t
      -> impl module_ctxt
  | InSig : (module_declaration list * module_declaration list) Option.t
      -> intf module_ctxt

type _ module_type_ctxt =
  | InStr : impl module_type_ctxt
  | InSig : intf module_type_ctxt

type (_, _) item_element =
  | Value         : 'a value_ctxt  * 'a value_view       -> (_value,          'a) item_element
  | Structure     : 'a module_ctxt * 'a module_view      -> (_structure,      'a) item_element
  | Functor       : 'a module_ctxt * 'a module_view      -> (_functor,        'a) item_element
  | StructureType :                  'a module_type_view -> (_structure_type, 'a) item_element
  | FunctorType   :                  'a module_type_view -> (_functor_type,   'a) item_element

val sort : 'a root_view -> 'a sort

val item_element : (Ident.t, 'a) Atom.t -> 'b item_view -> ('a, 'b) item_element Option.t
val item_description_ctxt : 'a item_view -> 'a item_description_ctxt
val item_ctxt : 'a sort -> ('a item_view list * 'a item_view list) -> 'a item_ctxt
val item_list_ctxt : 'a root_view -> 'a item_list_ctxt

val items : 'a root_view -> 'a item_view list

val initial_env : 'a root_view -> Env.t
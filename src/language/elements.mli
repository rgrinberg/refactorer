open Containers

open Compiler

module Base : sig

  (** Types denoting the implementation/interface dichotomy. *)

  (* An implementation is a module; an interface is a **named** module type. *)

  type impl = Impl
  (** Implementation level type tag. *)
  type intf = Intf
  (** Interface level type tag. *)

  type _ sort =
    | Impl : impl sort
    | Intf : intf sort

  (** Primary language constructs - these types are prefixed with an underscore in
      order to prevent their names clashing with keywords. *)

  type _value = Value

  (* We make a distinction between structures and functors, and similarly with their types. *)
  type _structure = Structure
  type _structure_type = StructureType
  type _functor = Functor
  type _functor_type = FunctorType

  type _parameter = Parameter

  type _ t =
    | Value         :                 _value          t
    | Parameter     :                 _parameter      t
    | Functor       :                 _functor        t
    | FunctorType   :                 _functor_type   t
    | Structure     :                 _structure      t
    | StructureType :                 _structure_type t
    (* | Module        : _module      -> _module         t
    | ModuleType    : _module_type -> _module_type    t *)
  (** The (generalised) abstract type of language elements. *)

  type _t = Ex : _ t -> _t
  (** The existentially quantified abstract type of language elements. *)

  val equal : 'a t -> 'b t -> bool
  (** Equality between two abstract language element values. *)

  val pp : ('a t) Format.printer
  (** Pretty printer for element kinds. *)

  val _pp : _t Format.printer
  (** Pretty printer for existential element kinds. *)

  exception KindMismatch of string
  (** Exception indicating that a value is not the right kind of element
      (value, module, etc.) *)

end
(** This module contains types denoting the various elements found in the
    language, e.g. values, modules, module types, classes, etc.

    Each type consists of just a single nullary constructor - they are
    essentially type-level tags. *)

open Base

module type Compound_Sig = sig
  type 'a sub_t
  val lift : 'a sub_t -> 'a t
  val pp : 'a sub_t Format.printer
  type _sub_t = Ex : 'a sub_t -> _sub_t
  val _lift : _sub_t -> _t
end

module Module : sig
  type _ sub_t =
    | S : _structure sub_t
    | F : _functor   sub_t
  (** Modules can be either structures or functors. *)
  include Compound_Sig with type 'a sub_t := 'a sub_t
  module type T0 = sig
    type 'a data
    type module_t =
    | S of _structure data
    | F of _functor data
  end
  module Make_T0 (X : sig type 'a data end) : sig
    include T0 with type 'a data := 'a X.data
  end
  module type T1 = sig
    type ('a, 'b) data
    type 'a module_t =
    | S of ('a, _structure) data
    | F of ('a, _functor) data
  end
  module Make_T1 (X : sig type ('a, 'b) data end) : sig
    include T1 with type ('a, 'b) data := ('a, 'b) X.data
  end
end
(** A module for representing the concept of a module, which is really a
    compound comprising a sum of structures and functors. *)

module ModuleType : sig
  type _ sub_t =
    | ST : _structure_type sub_t
    | FT : _functor_type   sub_t
  (** Module types can be either structure types or functor types. *)
  include Compound_Sig with type 'a sub_t := 'a sub_t
  val type_of : _sub_t -> Module._sub_t
  module type T0 = sig
    type 'a data
    type module_type_t =
    | ST of _structure_type data
    | FT of _functor_type data
  end
  module Make_T0 (X : sig type 'a data end) : sig
    include T0 with type 'a data := 'a X.data
  end
  module type T1 = sig
    type ('a, 'b) data
    type 'a module_type_t =
    | ST of ('a, _structure_type) data
    | FT of ('a, _functor_type) data
  end
  module Make_T1 (X : sig type ('a, 'b) data end) : sig
    include T1 with type ('a, 'b) data := ('a, 'b) X.data
  end
end
(** A module for representing the concept of a module type, which is really a
    compound comprising a sum of structure types and functor types. *)
open Containers
open   Format
open   Fun
open   IO

open Buildenv
open Sourcefile
open Fileinfos
open Lib

open Compiler
open   Config

include Fileinfos.Set
(* TODO: Implement something more efficient for a filesystem,
         e.g. a btree that mirrors the file structure. *)

let add_directory ~lib ~ppx ~dir cb =
  let printer = (fun f () -> fprintf f "Adding directory %s" dir) in
  if not (Sys.is_directory dir) then
    let () = Logging.info @@ fun _f -> _f "%a" printer () in
    raise Not_found
  else
    let files = Sys.readdir dir in
    let printer =
      (fun f () -> fprintf f
        "%a@,@[<v 4>Found %i items" printer () (Array.length files)) in
    let files, printer =
      (([], printer), files)
        |> uncurry @@ Array.fold @@ fun (fs, printer) f ->
        let f = Filename.concat dir f in
        try 
          if Sys.is_directory f then
            fs,
            (fun _f () -> fprintf _f "%a@,%s is a subdirectory" printer () f)  
          else if is_ocaml f then
            (Fileinfos.mk (f, lib, ppx) :: fs), printer
          else
            fs,
            (fun _f () -> fprintf _f "%a@,%s is not an ocaml file" printer () f)
        with Not_found ->
          fs,
          (fun _f () -> fprintf _f "%a@,%s was not found" printer () f) in
    let printer =
      (fun f () -> fprintf f "%a@]@,@[<v 4>Filtered down to %i files:@,%a@]"
        printer () (List.length files)
        (List.pp Fileinfos.pp_filename) files) in
    let () = Logging.info @@ fun _f -> _f "%a" printer () in
    add_list cb files

let match_file ~suf f =
  let f = f.filename in
  String.suffix ~suf f
    && String.equal (Filename.basename suf) (Filename.basename f)
  (* TODO: There is still a potential bug here because we don't make sure that
           the suffix match is only carried out on subdirectory boundaries. Some
           e.g. foo/bar/baz.ml could still match with etc/blah_foo/bar/baz.ml *)

let find_file ?lib suf fs =
  let pred =
    Option.map_or ~default:(match_file ~suf)
      (fun lib f ->
        match_file ~suf f && Option.equal String.equal lib f.library)
      (lib) in
  List.find_pred pred (to_list fs)

let find_all_files suf fs =
  to_list (filter (match_file ~suf) fs)

module CmtMap = CCHashtbl.Make(String)

let modname_map fs =
  let num_files = cardinal fs in
  let map = CmtMap.create (num_files / 2) in
  let add f =
    let mod_name = cmt_module_name f in
    let update entry f =
      match Filename.extension (Fileinfos.filename f) with
      | ext when String.equal ext !implementation_suffix ->
        begin match entry with
        | (None, r) ->
          (Some f, r)
        | (Some f', _) ->
          failwith
            (Format.sprintf "%s.modname_map: %s already has implementation %s"
              __MODULE__ mod_name (Fileinfos.filename f'))
        end
      | ext when String.equal ext !interface_suffix ->
        begin match entry with
        | (l, None) ->
          (l, Some f)
        | (_, Some f') ->
          failwith
            (Format.sprintf "%s.modname_map: %s already has interface %s"
              __MODULE__ mod_name (Fileinfos.filename f'))
        end
      | _ ->
        failwith
          (Format.sprintf "%s.modname_map: unexpected extension %s"
            __MODULE__ (Fileinfos.filename f)) in
    CmtMap.replace map mod_name
      (update (CmtMap.get_or ~default:(None, None) map mod_name) f) in
  let () = iter add fs in
  map

(* Parses a sequence of items of the form <mod_name>:(ml|mli) [ <mod_name>* ] *)
let parse_deps s =
  let open MParser in
  let open MParser_PCRE in
  let parse_triple s =
    let implementation_suffix =
      Option.get_exn (String.chop_prefix ~pre:"." !implementation_suffix) in
    let interface_suffix =
      Option.get_exn (String.chop_prefix ~pre:"." !interface_suffix) in
    ((Identifier.parse_capitalized_ident << spaces) >>= (fun modname ->
     Tokens.colon >>
     (attempt (string interface_suffix >>$ `Intf)
       <|>
     (string implementation_suffix >>$ `Impl)) >>= (fun sort ->
     spaces >>
     (Tokens.squares
       (many (Identifier.parse_capitalized_ident << spaces))) >>= (fun deps ->
     return (sort, modname, deps))))
    ) s in
  (spaces >> (many (parse_triple << spaces))) s

let compute_deps fs =
  let get_deps f acc =
    let deps =
      (* try
        Fileinfos.get_module_deps f
      with Not_found -> *)
      try
        Sourcefile.get_module_deps (of_fileinfos f)
      with Invalid_argument _ ->
        (* TODO: Eventually, need to handle case when typed tree not available? *)
        let () = Logging.err @@ fun _f -> _f
          ~header:"Module deps" "No typed AST for %s" f.filename in
        failwith (Format.sprintf "No typed AST for %s" f.filename) in
    let () = Logging.info @@ fun _f -> _f
      "@[<v 4>Found %i module dependencies for %a%a%a@]"
        (List.length deps)
        pp_filename f
        (if List.is_empty deps then silent else return "@,") ()
        (List.pp pp_print_string) deps in
    let () = Logging.minor_progress () in
    let sort =
      match Filename.extension (Fileinfos.filename f) with
      | ext when String.equal ext !implementation_suffix -> `Impl
      | ext when String.equal ext !interface_suffix      -> `Intf
      | _ ->
        failwith
          (Format.sprintf "%s.compute_deps: unexpected extension %s"
            __MODULE__ (Fileinfos.filename f)) in
    (sort, cmt_module_name f, deps) :: acc in
  fold get_deps fs []

let dependency_graph ?from_file fs =
  let mod_deps =
    match from_file with
    | None ->
      let () =
        Logging.progress_statement @@ fun _f -> _f
          "Calculating module dependencies " in
      compute_deps fs
    | Some f ->
      let () =
        Logging.progress_statement @@ fun _f -> _f
          "Reading module dependencies" in
      IO.with_in f (mk_of_channel parse_deps) in
  let () = Logging.major_progress () in
  let () =
    Logging.progress_statement @@ fun _f -> _f
      "Calculating file dependencies " in
  let graph = Graph.create ~size:(cardinal fs) () in
  let () = iter (Graph.add_vertex graph) fs in
  let map = modname_map fs in
  let add_deps_to_graph (sort, mod_name, deps) =
    match sort, CmtMap.get_or ~default:(None, None) map mod_name with
    | `Intf, (_, Some f) | `Impl, (Some f, _) ->
      let deps =
        let lookup =
          ((CmtMap.get_or ~default:(None, None) map)
            %> (Pair.merge (fun x y -> [x; y]))
            %> (List.filter_map id)) in
        List.flat_map lookup deps in
      let () =
        if not (List.is_empty deps) then
          Logging.info @@ fun _f -> _f
            "@[<v 4>Found the following file dependencies for %a:@,%a@]"
            pp_filename f
            (List.pp pp_filename) deps in
      let () = List.iter (Graph.add_edge graph f) deps in
      Logging.minor_progress ()
    | _ -> () in
  let () = List.iter add_deps_to_graph mod_deps in
  let () = Logging.major_progress () in
  graph

(* Command-line terms for specifying codebases *)

let add_item cb spec =
  match List.rev spec with
  | [] -> invalid_arg "Spec cannot be empty!"
  | input :: rest ->
    let ppx =
      List.filter_map
        (fun s ->
          Option.return_if
            (String.prefix ~pre:"ppx:" s)
            (String.sub s 4 ((String.length s) - 4)))
        (rest) in
    let ppx = List.rev ppx in
    let lib =
      Option.map
        (fun s -> String.sub s 4 ((String.length s) - 4))
        (List.find_opt (String.prefix ~pre:"lib:") rest) in
    let input = if Sys.is_directory input then `Dir input else `File input in
    let () = Option.iter (Buildenv.register_lib ?loc:(Some input)) lib in
    match input with
    | `File f ->
      begin try
        add (Fileinfos.mk (f, lib, ppx)) cb
      with Not_found ->
        let () = Logging.warn @@ fun _f -> _f "%s not found!" f in
        cb
      end
    | `Dir dir ->
      begin try
        add_directory ~lib ~ppx ~dir cb
      with Not_found ->
        let () = Logging.warn @@ fun _f -> _f "%s not found!" dir in
        cb
      end

let from_spec =
  List.fold_left add_item empty

let codebase_man_section = "OPTIONS FOR SPECIFYING THE CODEBASE"

let spec =
  let spec_info =
    let docs = codebase_man_section in
    let doc = "Add $(docv) to the codebase; $(docv) must be of the form " ^
              "[ppx:PATH,]*[lib:NAME,]?PATH" in
    let docv = "ITEM" in
    Cmdliner.Arg.info ~docs ~doc ~docv ["i"; "input"] in
  (* let spec_conv = Cmdliner.Arg.conv (parse_item, print_item) in *)
  Cmdliner.Arg.(value & opt_all (list string) [] spec_info)

let deps_from =
  let docs = codebase_man_section in
  let doc = "Load module dependencies from $(docv)" in
  let docv = "FILE" in
  let arg_info = Cmdliner.Arg.info ~docs ~doc ~docv ["deps-from"] in
  Cmdliner.Arg.(value & opt (some file) None arg_info)

let module_opt =
  let docs = Cmdliner.Manpage.s_common_options in
  let doc = "Use $(docv) for the module name of code read from standard input" in
  let docv = "MODNAME" in
  let arg_info = Cmdliner.Arg.info ~docs ~doc ~docv ["module"] in
  Cmdliner.Arg.(value & opt (some string) None arg_info)

let stdin_kind =
  let docs = Cmdliner.Manpage.s_common_options in
  let docv = "KIND" in
  let impl_flag =
    let doc = "Interpret code read from standard input as an implementation." in
    Cmdliner.Arg.info ~docs ~doc ~docv ["impl"] in
  let intf_flag =
    let doc = "Interpret code read from standard input as an interface." in
    Cmdliner.Arg.info ~docs ~doc ~docv ["intf"] in
  let flags = [(Some `Impl, impl_flag); (Some `Intf, intf_flag)] in
  Cmdliner.Arg.(value & vflag None flags)

let stdin_lib =
  let docs = Cmdliner.Manpage.s_common_options in
  let doc = "Interpret code read from standard input as belonging to library $(docv)" in
  let docv = "LIB" in
  let arg_info = Cmdliner.Arg.info ~docs ~doc ~docv ["lib"] in
  Cmdliner.Arg.(value & opt (some string) None arg_info)

let codebase stdin_module stdin_kind stdin_lib spec use_merlin prefix =
  let merlin_paths = if use_merlin then Merlinfile.source_dirs () else [] in
  let spec =
    if List.is_empty spec && (not use_merlin) then
      let mod_name, ext =
        match stdin_module, stdin_kind with
        | None, None ->
          failwith "A module name and kind must both be specified when using standard input!"
        | None, _ ->
          failwith "A module name must be specified when using standard input!"
        | _, None ->
          failwith "A input kind must be specified when using standard input!"
        | Some mod_name, Some `Impl ->
          mod_name, !implementation_suffix
        | Some mod_name, Some `Intf ->
          mod_name, !interface_suffix in
      let tmp_file =
        Filename.concat (tmp_dir prefix) (sprintf "%s.%s" mod_name ext) in
      let tmp_file_out = open_out tmp_file in
      let () = output_string tmp_file_out (read_all stdin) in
      let () = close_out tmp_file_out in
      let input = [tmp_file] in
      let input =
        Option.map_or ~default:input
          (fun lib -> ("lib:" ^ lib) :: input) stdin_lib in
      [input]
    else
      spec in
  let cb = from_spec spec in
  let cb = 
    List.fold_right 
      (fun dir -> add_directory ~lib:None ~ppx:[] ~dir) merlin_paths cb in
  let () =
    Logging.major_progress ~msgf:(fun _f -> _f
      "Codebase contains %i files" (cardinal cb)) () in
  cb

let codebase_with_deps =
  fun stdin_module stdin_kind stdin_lib from_file spec use_merlin prefix ->
    let cb = 
      codebase stdin_module stdin_kind stdin_lib spec use_merlin prefix in
    let deps = dependency_graph ?from_file cb in
    cb, deps

let of_cmdline =
  let open Cmdliner.Term in
    const codebase
      $ module_opt
      $ stdin_kind
      $ stdin_lib
      $ spec
      $ Merlinfile.use

let of_cmdline_with_deps =
  let open Cmdliner.Term in
    const codebase_with_deps
      $ module_opt
      $ stdin_kind
      $ stdin_lib
      $ deps_from
      $ spec
      $ Merlinfile.use
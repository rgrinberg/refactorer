open Containers
open Format

open Compiler

open Longident
open Path

open Compenv
open Config

open Ast_lib

type library_name = string
type file_name = string

module LibMap = Hashtbl.Make'(String)

type libinfo = {
    user_entrypoint : bool ;
  }

let has_entrypoint { user_entrypoint } = user_entrypoint

let libs : libinfo LibMap.t = LibMap.create 1

let clear () = LibMap.clear libs

let implementation_suffix = ref ".ml"

let register_lib ?loc lib =
  let lib = String.uncapitalize_ascii lib in
  let user_entrypoint =
    match LibMap.get libs lib, loc with
    | Some lib_info, _ when has_entrypoint lib_info ->
      true
    | _, Some `File f ->
      String.equal (Filename.extension f) !implementation_suffix
        &&
      String.equal
        (String.lowercase_ascii lib)
        (String.lowercase_ascii
          (Filename.remove_extension (Filename.basename f)))
    | _, Some `Dir d ->
      Sys.file_exists
        (Filename.concat d
          ((String.lowercase_ascii lib) ^ !implementation_suffix))
    | _ ->
      false in
  LibMap.replace libs lib { user_entrypoint }

let module_of_filename f =
  let basename = Filename.basename f in
  let name =
    try
      let pos = String.index basename '.' in
      String.sub basename 0 pos
    with Not_found -> basename in
  String.capitalize_ascii name

let rec remove_exts f =
  let f' = Filename.remove_extension f in
  if String.equal f f' then f else remove_exts f'

let build_filename lib file ext =
  let fn_no_ext = remove_exts (Filename.basename file) in
  lib
    |> Option.map_or ~default:(fn_no_ext ^ ext) @@ fun lib_name ->
    (* Reproduce filename mangling used by Jane Street *)
    let prefix = lib_name ^ "__" in
    let module_name = module_of_filename fn_no_ext in
    let fn_no_ext =
      if String.equal
          (String.lowercase_ascii lib_name)
          (String.lowercase_ascii module_name)
      then (String.lowercase_ascii module_name)
      else prefix ^ module_name in
    fn_no_ext ^ ext

let is_ocaml f =
  let matches = String.equal (Filename.extension f) in
  matches !implementation_suffix
    ||
  matches !Config.interface_suffix

let cmt_filename ~lib ~file =
  let ext =
    if (Filename.check_suffix file !interface_suffix)
      then ".cmti" else ".cmt" in
  build_filename lib file ext
let object_filename ~lib ~file =
  let ext =
    if (Filename.check_suffix file !interface_suffix)
      then ".cmi" else ".cmo" in
  build_filename lib file ext

module Factorise = struct

  let source_info id =
    match List.hd_tl (String.split ~by:"__" id) with
    | id, [] ->
      (* TODO: We should be a little cleverer here - we should take into account
               dependencies between libraries (gleaned from jbuilder files) and
               what library context this function is being called in. That is,
               if the head of the path has the same name as some library
               registered with this module, that does not necessarily mean it is
               referring to the entry module of that library - this would only
               be the case if the library context of the path depends on that
               library. *)
      begin match (List.hd_tl (String.split ~by:"." id)) with
      | id, [] when LibMap.mem libs (String.uncapitalize_ascii id) ->
        Some (String.uncapitalize_ascii id), id
      | _ ->
        None, id
      end
    | lib, ss ->
      Some (String.uncapitalize_ascii lib), String.concat "__" ss

  let longident id =
    match Longident.flatten id with
    | [] ->
      assert false
    | head :: ids ->
      let lib, head = source_info head in
      let id = List.fold_left (fun lid id -> Ldot(lid, id)) (Lident head) ids in
      lib, id

  let source_info id =
    let () =
      if not (Ident.persistent id) then
        invalid_arg
          (sprintf "%s.Factorise.source_info" __MODULE__) in
    source_info (Ident.name id)

    (* TODO: Fold the call to source_info into the locally defined recursive
          function fix_path, to improve efficiency. Actually, this would be
          folding the call to Path.head. *)
  let path p =
    let lib, head = source_info (Path.head p) in
    let rec fix_path =
      function
      | Pident _ ->
        Pident (Ident.create_persistent head)
      | (Pdot _) as p ->
        dot_map ~hd:fix_path p
      | Papply (p, p') ->
        Papply ((fix_path p), p') in
    lib, (fix_path p)

end

let source_info = Factorise.source_info

(* I think JBuilder has changed now so that it does not compile a library
   [lib]'s entry module [mod] into a file called [lib__mod], but just as
   [mod]; so this may be redundant. *)
let rec flatten_path =
  let open Path in
  function
  | (Pdot _) as p ->
    begin match dest_dot p with
    | Pident lib, _mod ->
      let name =
        if String.equal (Ident.name lib) (_mod ^ "__") then
          _mod
        else if String.suffix ~suf:"__" (Ident.name lib) then
          (Ident.name lib) ^ _mod
        else
          (Ident.name lib) ^ "__" ^ _mod in
      Pident (Ident.with_name lib name)
    | _ -> 
      dot_map ~hd:flatten_path p
    end
  | p -> p

let wrap_lookup f ?backup_lib (lib, lid) =
  let lookup_in_lib lib =
    let lib =
      match LibMap.get libs lib with
      | Some lib_info when has_entrypoint lib_info ->
        lib ^ "__"
      | _ ->
        lib in
    flatten_path
      (f (Longident.append (Lident (String.capitalize_ascii lib)) lid)) in
  let raw_lookup = lazy (f lid) in
  (* If the long identifier to look up is explicitly given to be part of a
     library, then include that library in the lookup path. Otherwise, first try
     looking up the identifier in the backup library if one is given, and if
     this is not given or that lookup fails then fall back on a lookup without a
     library. *)
  match lib, backup_lib with
  | Some lib, _    -> lookup_in_lib lib
  | None, None     -> Lazy.force raw_lookup
  | None, Some lib ->
      try lookup_in_lib lib
      with Not_found -> Lazy.force raw_lookup

(* -------------------------------------------------------------------------- *)
(* Occurrences of the following types in type definitions have been changed   *)
(* to use the types indicated in parentheses so that the visitor generation   *)
(* code creates calls to the correct method.                                  *)
(*      Location.t      (Location_visitors.location_t)                        *)
(* -------------------------------------------------------------------------- *)

open Compiler

(* Asttypes has no implementation, so we cannot declare a module alias
     module Base = Asttypes     *)

type constant = Asttypes.constant =
    Const_int of int
  | Const_char of char
  | Const_string of string * string option
  | Const_float of string
  | Const_int32 of int32
  | Const_int64 of int64
  | Const_nativeint of nativeint

and rec_flag = Asttypes.rec_flag =
  Nonrecursive | Recursive

and direction_flag = Asttypes.direction_flag =
  Upto | Downto

and private_flag = Asttypes.private_flag =
  Private | Public

and mutable_flag = Asttypes.mutable_flag =
  Immutable | Mutable

and virtual_flag = Asttypes.virtual_flag =
  Virtual | Concrete

and override_flag = Asttypes.override_flag =
  Override | Fresh

and closed_flag = Asttypes.closed_flag =
  Closed | Open

and label = string

and arg_label = Asttypes.arg_label =
    Nolabel
  | Labelled of string (*  label:T -> ... *)
  | Optional of string (* ?label:T -> ... *)

and 'a loc = 'a Location.loc = {
  txt : 'a;
  loc : Location_visitors.location_t;
}

and variance = Asttypes.variance =
  | Covariant
  | Contravariant
  | Invariant

[@@deriving
    visitors { variety = "iter"; polymorphic = true; monomorphic = ["'env"]; ancestors = [ "Location_visitors.iter" ] }
  , visitors { variety = "map"; polymorphic = true; monomorphic = ["'env"]; ancestors = [ "Location_visitors.map" ] }
  , visitors { variety = "reduce"; polymorphic = true; monomorphic = ["'env"]; ancestors = [ "Location_visitors.reduce" ] }

  , visitors { variety = "iter2"; concrete = true; polymorphic = true; monomorphic = ["'env"]; ancestors = [ "Location_visitors.iter2" ] }
]
open Containers
open Compiler

type t = Logs.src

val cmdline_opts : (Logs.level option * string option * bool) Cmdliner.Term.t

val set_logging : (Logs.level option * string option * bool) -> unit

val log : t

val reporter : ?delegate:Logs.reporter -> unit -> Logs.reporter

include Logs.LOG

module TagDefs : sig
  val loc : Location.t Logs.Tag.def
  val preamble : unit Format.printer Logs.Tag.def
end

module Tags : sig
  val of_loc : Location.t -> Logs.Tag.set
  val preamble : string -> Logs.Tag.set
end

val progress_statement : ?log:bool -> ('a, unit) Logs.msgf -> unit
(** If the [show_progress] flag is set, then [minor_progress msgf] uses [msgf]
    to output to the standard error. *)

val minor_progress : ?msgf:('a, unit) Logs.msgf -> unit -> unit
(** If the [show_progress] flag is set, then [minor_progress ()] outputs to
    standard error indicating that minor progress has been made. By default,
    this is the '.' character; however if the option [msgf] parameter is
    provided, then this message function is used for the output. In this case,
    the message is also send to the logging source [log] as an [info] message. *)

val major_progress : ?msgf:('a, unit) Logs.msgf -> unit -> unit
(** If the [show_progress] flag is set, then [major_progress ()] outputs to
    standard error indicating that minor progress has been made. By default,
    this is the string " done" followed by a break hint; however if the option
    [msgf] parameter is provided, then this message function is used for the
    output. In this case, the message is also send to the logging source [log]
    as an [info] message. *)

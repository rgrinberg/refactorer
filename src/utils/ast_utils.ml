open Containers

open Compiler
open  Typedtree
open  Asttypes

open Ident

module ModuleBinding = Module.Binding

open Elements
open Typedtree_views
open Identifier
open Moduletype

open Visitors_lib

open Lib

(* TODO: Implement this by building up a [Modulescope.t] as the visitor
         environment, then we can find bindings deep inside anonymous module
         includes.
   QUESTION: Do we really want to do this? We need to figure out a comprehensive
             and correct treatment of anonymous modules. *)
let find_path_to_binding (type a b)
    (id : (Ident.t, a) Atom.t) (modname, (root : b root_view)) =
  let reducer =
    let process_include env incl_type links =
      if contains env incl_type id then
        Some (Chain.build links)
      else
        None in
    object(self)
      inherit [_] Opt_reducers.leftmost as super
      (* TODO: Look for value, type, exception and class bindings. *)
      (* TODO: Look for parameter bindings.
               Perhaps we do not need to look for parameter bindings, as these
               are searched for specifically in the module scope. *)
      method! visit_tt_module_binding links mb =
        let sort = ModuleBinding.impl_sort mb in
        match id, sort with
        | Atom.Structure id, Some Module.(Ex S)
            when Ident.same mb.mb_id id ->
          Some (Chain.build links)
        | Atom.Functor id, Some Module.(Ex F)
            when Ident.same mb.mb_id id ->
          Some (Chain.build links)
        | _, Some sort ->
         self#visit_tt_module_expr
           ((Atom._mk (Module._lift sort) (Atom.Data.only (Ident.name mb.mb_id)))
             :: links)
           (mb.mb_expr)
        | _ ->
          None
      method! visit_tt_module_declaration links md =
        let sort = ModuleBinding.intf_sort md in
        match id, sort with
        | Atom.Structure id, Some Module.(Ex S)
            when Ident.same md.md_id id ->
          Some (Chain.build links)
        | Atom.Functor id, Some Module.(Ex F)
            when Ident.same md.md_id id ->
          Some (Chain.build links)
        | _, Some sort ->
         self#visit_tt_module_type
           ((Atom._mk (Module._lift sort) (Atom.Data.only (Ident.name md.md_id)))
             :: links)
           (md.md_type)
        | _ ->
          None
      method! visit_tt_module_type_declaration links mtd =
        let sort = Moduletype.Binding.sort mtd in
        match id, sort with
        | Atom.StructureType id, Some ModuleType.(Ex ST)
            when Ident.same mtd.mtd_id id ->
          Some (Chain.build links)
        | Atom.FunctorType id, Some ModuleType.(Ex FT)
            when Ident.same mtd.mtd_id id ->
          Some (Chain.build links)
        | _, Some sort ->
          mtd.mtd_type
          |> Option.flat_map @@ fun mtd_type
          -> self#visit_tt_module_type
               ((Atom._mk
                 (ModuleType._lift sort)
                 (Atom.Data.only (Ident.name mtd.mtd_id)))
                   :: links)
               (mtd_type)
        | _ ->
          None
      method! visit_tt_include_declaration links infos =
        process_include
          (infos.incl_mod.mod_env)
          (Types_views.ST infos.incl_type)
          (links)
      method! visit_tt_include_description links infos =
        process_include
          (infos.incl_mod.mty_env)
          (Types_views.ST infos.incl_type)
          (links)
    end in
  let links =
    [ Atom.Ex (Atom.mk Elements.Base.Structure (Atom.Data.only modname)) ] in
  match root with
  | Str _struct ->
    reducer#visit_tt_structure links _struct
  | Sig _sig ->
    reducer#visit_tt_signature links _sig

type ('a, 'b) binding_site = [
    | `Binding of ('a, 'b) item_element
    | `Include of      'b  include_view
  ]

type ('a, 'b) binding_source =
  'b item_list_ctxt * 'b item_ctxt * 'b item_description_ctxt
    * ('a, 'b) binding_site

let pp_binding_site
      : type a b .
          [ `Binding of (a, b) item_element | `Include of b include_view ]
            Format.printer =
  fun fmt site ->
    let s =
      match site with
      | `Binding elem ->
        begin match elem with
        | Value (InStrPrim, _) -> "Primitive Value"
        | Value (_, _) -> "Value"
        | Structure (_, _) -> "Structure"
        | Functor (_, _) -> "Functor"
        | StructureType _ -> "StructureType"
        | FunctorType _ -> "FunctorType"
        end
      | `Include _ -> "Include" in
    Format.fprintf fmt "%s" s

let pp_item_description_ctxt : type b . b item_description_ctxt Format.printer =
  fun fmt ->
    function
    | InStr { str_loc; _} ->
      Format.fprintf fmt "InStr %a" Location.print_loc str_loc
    | InSig { sig_loc; _ } ->
      Format.fprintf fmt "InSig %a" Location.print_loc sig_loc

let pp_binding_source : type a b . (a, b) binding_source Format.printer =
  fun fmt (_, _, item_ctxt, site) ->
    Format.fprintf fmt "%a %a"
      pp_binding_site site
      pp_item_description_ctxt item_ctxt

let find_binding
    (type a) (id : (Ident.t, a) Atom.t)
    (type b) (root: b root_view)
      : (a, b) binding_source Option.t =
  let items = items root in
  let items =
    (* Note that when looking for value bindings, we reverse the item list
       because we want to find the last binding of the identifier. *)
    match id with
    | Atom.Value _ -> List.rev items
    | _ -> items in
  items
  |> pivot_map (fun (item : b item_view) ->
      match item_element id item with
      | Some elem -> Some ((`Binding elem), item_description_ctxt item)
      | None ->
        let incl =
          match item with
          | InStr { str_desc = Tstr_include incl; str_env; _ }
              when contains str_env (Types_views.ST incl.incl_type) id ->
            Some (`Include ((InStr incl) : b include_view))
          | InSig { sig_desc = Tsig_include incl; sig_env; _ }
              when contains sig_env (Types_views.ST incl.incl_type) id ->
            Some (`Include ((InSig incl) : b include_view))
          | _ ->
            None in
        incl
        |> Option.map (fun incl
        -> incl, item_description_ctxt item))
  |> Option.map @@ fun (l, (site, desc_ctxt), r)
  -> let items =
       match id with
       | Atom.Value _ ->
         (* Note [l] and [r] swapped around to compensate for initial reversing of the item list. *)
         (r, l)
       | _ -> (l, r) in
     (item_list_ctxt root)
     , (item_ctxt (Typedtree_views.sort root) items)
     , desc_ctxt
     , site

let next_env (type a b) (src  : (a, b) binding_source) =
  match src with
  | _, _, InStr { str_env; _ }, `Binding Value (InStrVal (Recursive, _), _) ->
    str_env
  | _, _, InStr { str_env; _ }, `Binding Structure (InStr Some _, _) ->
    str_env
  | _, _, InStr { str_env; _ }, `Binding Functor (InStr Some _, _) ->
    str_env
  | InStr { str_final_env; _ }, InStr (_, []), _, _ ->
    str_final_env
  | InStr { str_final_env; _ }, InStr (_, { str_env; _ } :: _), _, _ ->
    str_env
  | InSig { sig_final_env; _ }, InSig (_, []), _, _ ->
    sig_final_env
  | InSig { sig_final_env; _ }, InSig (_, { sig_env; _ } :: _), _, _ ->
    sig_env

let next_items (type a b)
    ((_, item_ctxt, item_desc_ctxt, binding) : (a, b) binding_source) =
  match binding with
  | `Binding Value (InStrVal (Recursive, (vs, vs')), InStrVal v) ->
    let InStr (_, rest) = item_ctxt in
    let InStr { str_env; str_loc; } = item_desc_ctxt in
    let item =
      { str_desc = Tstr_value (Recursive, vs @ v::vs'); str_loc; str_env; } in
    (InStr (item :: rest) : b items_view)
  | `Binding Structure (InStr Some (mbs, mbs'), InStr mb) ->
    let InStr (_, rest) = item_ctxt in
    let InStr { str_env; str_loc; } = item_desc_ctxt in
    let item =
      { str_desc = Tstr_recmodule (mbs @ mb::mbs'); str_loc; str_env; } in
    (InStr (item :: rest) : b items_view)
  | `Binding Functor (InStr Some (mbs, mbs'), InStr mb) ->
    let InStr (_, rest) = item_ctxt in
    let InStr { str_env; str_loc; } = item_desc_ctxt in
    let item =
      { str_desc = Tstr_recmodule (mbs @ mb::mbs'); str_loc; str_env; } in
    (InStr (item :: rest) : b items_view)
  | _ ->
    match item_ctxt with
    | InStr (_, rest) -> InStr rest
    | InSig (_, rest) -> InSig rest

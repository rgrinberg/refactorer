Internal Test Suite
----

This test suite consists of refactoring "unit" tests, in the sense that each
such unit test consists of a small number of source files that reside in a
single directory, and test a small, basic piece of refactoring functionality.

The immediate subdirectories indicate which refactoring is tested by the tests
they contain. The name of the directory is used by the automated test scripts
as the name of the refactoring. Each further subdirectory comprises a single
unit test for that refactoring. Each such unit test directory must contain a
Makefile, the default target for which should build the source files in the
directory. The Makefile should also contain a `clean` target. Each unit test
directory must also contain a file named `params` which contain the parameters
that the automated scripts will pass to the refactoring; the parameters must all
be on a single line, and separated by whitespace. The directory must also
contain a file named `test.expect`, consisting of a `diff` which is the expected
result of applying the refactoring, and which the automated test framework will
use to determine if the test has passed or failed.

Each refactoring suite main subdirectory contains its own `README.md` file
documenting the unit tests it contains.

See the main test suite [README](../README.md) file for details about how to
automatically run the tests.
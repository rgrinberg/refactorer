open Format

open Compiler

open Compmisc

open Frontend
open Sourcefile
open Fileinfos

open Logging

open Containers.Fun

let pp_flag =
  let doc = "Pretty-print the AST." in
  let arg_info = Cmdliner.Arg.info ~doc ["pp"] in
  Cmdliner.Arg.(value & flag arg_info)

let run codebase pp =
  let print_intf = if pp then Pprintast.signature else Printast.interface in
  let print_impl = if pp then Pprintast.structure else Printast.implementation in
  codebase |> Codebase.iter @@ of_fileinfos %> (fun f ->
  print_endline f.fileinfos.filename ;
  match f.ast with
  | Interface (Some _sig, _) ->
      print_intf std_formatter _sig
  | Implementation (Some _struct, _) ->
      print_impl std_formatter _struct
  | _ ->
      prerr_endline "No AST found!")

let cmd =
  let open Cmdliner.Term in
  (with_common_opts (const run $ (Codebase.of_cmdline $ const !Configuration.tool_name) $ pp_flag)),
  (info "Parsetree Printer")

let () =
  Cmdliner.Term.(exit @@ eval cmd)

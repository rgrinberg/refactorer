open Containers

open Compiler

open Compmisc
open Config

open Location
open Typedtree

open Frontend

open Fileinfos
open Sourcefile

open VisitorsRuntime

let dir1 =
  Cmdliner.Arg.(required & pos 0 (some dir) None & info ~doc:"Left-hand directory" [])
let dir2 =
  Cmdliner.Arg.(required & pos 1 (some dir) None & info ~doc:"Right-hand directory" [])

let run codebase dir1 dir2 =
  let differs = ref false in
  let cmp_up_to_ghost_locs =
    object(self)
      inherit [_] Typedtree_visitors.iter2 as super
      method! visit_location_t env l l' =
        let { loc_start = _start; loc_end = _end; loc_ghost = ghost; } = l in
        let { loc_start = _start'; loc_end = _end'; loc_ghost = ghost' } = l' in
        let () = if Bool.equal ghost ghost' then differs := true in
        let () = self#visit_position env _start _start' in
        let () = self#visit_position env _end _end' in
        ()
    end in
  let print_diffs =
    object
      inherit [_] Typedtree_visitors.iter2 as super
      method! visit_tt_structure_item env item item' =
        let { str_loc = { loc_ghost = ghost; _ }; _ } = item in
        let { str_loc = { loc_ghost = ghost'; _ }; _ } = item' in
        if (not ghost) && ghost' then
          Printtyped.implementation Format.std_formatter
            { str_items = [ item ]; str_type = []; str_final_env = Env.empty; }
        else
          super#visit_tt_structure_item env item item'
      method! visit_tt_signature_item env item item' =
        let { sig_loc = { loc_ghost = ghost; _ }; _ } = item in
        let { sig_loc = { loc_ghost = ghost'; _ }; _ } = item; in
        if (not ghost) && ghost' then
          Printtyped.interface Format.std_formatter
            { sig_items = [ item ]; sig_type = []; sig_final_env = Env.empty; }
        else
          super#visit_tt_signature_item env item item'
    end in
  codebase |> Codebase.iter @@ fun f ->
  let () = print_string (Format.sprintf "%s: " f.filename) in
  let () = Sys.chdir dir1 in
  let src = of_fileinfos ~use_cache:false f in
  let () = Sys.chdir dir2 in
  let src' = of_fileinfos ~use_cache:false f in
  let () = differs := false in
  match src.ast, src'.ast with
  | Implementation (_, Some ast), Implementation (_, Some ast') ->
    begin match cmp_up_to_ghost_locs#visit_tt_structure () ast ast' with
    | () ->
      let differs = !differs in
      let () =
        print_endline
          (Format.sprintf "ghost locations%s differ"
            (if not differs then " do not" else "")) in
      print_diffs#visit_tt_structure () ast ast'
    | exception StructuralMismatch ->
      print_endline "ASTs do not match!"
    end
  | Interface (_, Some ast), Interface (_, Some ast') ->
    begin match cmp_up_to_ghost_locs#visit_tt_signature () ast ast' with
    | () ->
      let differs = !differs in
      let () =
        print_endline
          (Format.sprintf "ghost locations%s differ"
            (if not differs then " do not" else "")) in
      print_diffs#visit_tt_signature () ast ast'
    | exception StructuralMismatch ->
      print_endline "ASTs do not match!"
    end
  | Implementation (_, None), Implementation _
  | Interface (_, None), Interface _ ->
    print_endline "left-hand typed AST missing!"
  | Implementation _, Implementation (_, None)
  | Interface _, Interface (_, None) ->
    print_endline "right-hand typed AST missing!"
    | _ ->
    print_endline "AST mismatch!"

let cmd =
  let open Cmdliner.Term in
  (with_common_opts (const run $ (Codebase.of_cmdline $ const !Configuration.tool_name) $ dir1 $ dir2)),
  (info "Value Use Counter")

let () =
  Cmdliner.Term.(exit @@ eval cmd)
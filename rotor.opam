opam-version: "2.0"
name: "rotor"
version: "0.1"
synopsis: "An automatic refactoring tool for OCaml"
description: """
ROTOR is a tool for automatically carrying out refactoring on multi-file OCaml
codebases. Currently, it supports renaming of value declarations.
"""
maintainer: "Reuben N. S. Rowe <reuben.rowe@cantab.net>"
authors: [ 
    "Reuben N. S. Rowe <reuben.rowe@cantab.net>"
    "Hugo Férée <hugo.feree@gmail.com>"
  ]
homepage: "https://trustworthy-refactoring.gitlab.io/refactorer"
bug-reports: "https://gitlab.com/trustworthy-refactoring/refactorer/issues"
dev-repo: "git+https://gitlab.com/trustworthy-refactoring/refactorer.git"
license: "MIT License"
depends: [
    "cmdliner"
    "cppo" { build }
    "cppo_ocamlbuild" { build }
    "containers" { >= "2.6" }
    "ocaml" { build & >= "4.04" & < "4.09.0" }
    "ocamlbuild" { build }
    "ocamlfind"
    "ocamlgraph" { >= "1.8.7" }
    "logs" { >= "0.6.2" }
    "pcre"
    "mparser" { >= "1.2.3" }
    "visitors"
  ]
depexts: [
  "diffutils"
  "patchutils"
]
build: [ make ]